<?php
return [
    'commands' => [
        \Balance\Commands\Worker::class,
        \Balance\Commands\SendEvent::class,
    ],
    'amqp' => [
        'server' => [
            'host' => 'localhost',
            'port' => 5672,
            'user' => 'guest',
            'password' => 'guest',
            'vhost' => '/',
            'exchangeName' => 'balance',
        ],
        'queues' => [
            \Balance\Workers\BalanceWorker::QUEUE_NAME
        ]
    ],
    'database' => [
        'master' => [
            'dbname' => 'balance',
            'user' => 'root',
            'password' => '2912',
        ]
    ]
];

#! /usr/bin/php
<?php
/**
 * Runs command
 */

$configuration = require __DIR__ . '/bootstrap.php';

$application = new Balance\Application\Cli($configuration);
$application->run();

<?php

require_once __DIR__ . '/../vendor/autoload.php';

$configuration = require __DIR__ . '/../config/main.php';

return $configuration;

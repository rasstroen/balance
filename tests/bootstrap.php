<?php
return [
    'commands' => [
        \Balance\Commands\Worker::class,
        \Balance\Commands\SendEvent::class,
    ],
    'amqp' => [
        'server' => [
            'host' => 'localhost',
            'port' => 5672,
            'user' => 'guest',
            'password' => 'guest',
            'vhost' => '/',
            'exchangeName' => 'balance_test',
        ],
        'queues' => [
            \Balance\Workers\BalanceWorker::QUEUE_NAME
        ]
    ],
    'database' => [
        'master' => [
            'dbname' => 'balance_test',
            'user' => 'root',
            'password' => '2912',
        ]
    ]
];

<?php

namespace Tests\Balance\Services;

use Balance\Workers\BalanceWorker;
use Tests\Balance\BaseTestCase;

/**
 *
 * @coversDefaultClass \Balance\Workers\BalanceWorker
 * @covers \Balance\Workers\BalanceWorker
 */
class BalanceWorkerTest extends BaseTestCase
{
    public function paramsDataProvider(): array
    {
        return [
            [[], 'Sender and recipient are not provided'],
            [['senderId' => 1], 'Amount is not provided'],
            [['senderId' => 1, 'amount' => 10], 'Transaction id is not provided'],
        ];
    }

    /**
     * Test process with illegal parameters
     *
     * @return void
     *
     * @covers ::process()
     *
     * @dataProvider paramsDataProvider
     */
    public function testIllegalParameters(array $params, string $exceptionMessage): void
    {
        $worker = new BalanceWorker($this->getDi());
        $this->expectExceptionMessage($exceptionMessage);
        $worker->process($params);
    }

    /**
     * @return void
     *
     * @covers ::process()
     *
     * @throws \Exception
     */
    public function testRun(): void
    {
        $transactionId = 100;
        $recipientId = 900;
        $amount = 12313;

        $worker = new BalanceWorker($this->getDi());

        $params = ['recipientId' => $recipientId, 'amount' => $amount, 'transactionId' => $transactionId];
        $worker->process($params);

        $transaction = $this->getDi()->transactionRepository->getById($transactionId);
        $recipient = $this->getDi()->userRepository->getOrCreate($recipientId);

        $this->assertEquals($amount, $recipient->getBalance());
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals($transactionId, $transaction->getId());
        $this->assertEquals(0, $transaction->getSenderId());
        $this->assertEquals($recipientId, $transaction->getRecipientId());
        $this->assertEquals(0, $transaction->getisBlocked());

        $params = ['senderId' => $recipientId, 'amount' => $amount, 'transactionId' => $transactionId + 1, 'isBlocked' => 1];
        $worker->process($params);

        $transaction = $this->getDi()->transactionRepository->getById($transactionId + 1);
        $sender = $this->getDi()->userRepository->getOrCreate($recipientId);

        $this->assertEquals(0, $sender->getBalance());
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals($transactionId + 1, $transaction->getId());
        $this->assertEquals(0, $transaction->getRecipientId());
        $this->assertEquals($recipientId, $transaction->getSenderId());
        $this->assertEquals(1, $transaction->getisBlocked());
    }
}

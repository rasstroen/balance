<?php

namespace Tests\Balance;

use Balance\Components\Database\DatabaseFactory;
use Balance\Components\ServiceLocator\ServiceLocator;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    /**
     * @var ServiceLocator
     */
    protected $dependencyInjection;

    /**
     * @return ServiceLocator
     */
    public function getDi()
    {
        return $this->dependencyInjection ?? $this->dependencyInjection = new ServiceLocator(require __DIR__ . '/../bootstrap.php');
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setUp()
    {
        $this->getDi()->database = new DatabaseFactory($this->getDi()->database->configuration);
        $this->getDi()->database->master->query('TRUNCATE transactions');
        $this->getDi()->database->master->query('TRUNCATE users');
        return parent::setUp();
    }
}

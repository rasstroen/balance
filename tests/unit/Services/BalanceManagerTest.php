<?php

namespace Tests\Balance\Services;

use Balance\Events\BalanceDecreasedEvent;
use Balance\Events\BalanceIncreasedEvent;
use Balance\Events\BalanceTransferredEvent;
use Balance\Services\BalanceManager;
use Balance\Services\Exceptions\DuplicateTransactionException;
use Tests\Balance\BaseTestCase;

/**
 *
 * @coversDefaultClass BalanceManager
 * @covers BalanceManager
 */
class BalanceManagerTest extends BaseTestCase
{
    /**
     * Test increasing user balance
     *
     * @return void
     *
     * @covers ::processTransaction()
     */
    public function testIncreaseBalance(): void
    {
        $transactionId = 1;
        $senderId = 0;
        $recipientId = 100;
        $amount = 100;
        $isBlocked = 0;

        $transaction = $this->getDi()->transactionRepository->getById($transactionId);
        $this->assertNull($transaction);

        // first transaction, creating user with balance = transaction amount
        $this->getDi()->balanceManager->processTransaction(
            $transactionId,
            $senderId,
            $recipientId,
            $amount,
            $isBlocked
        );

        $user = $this->getDi()->userRepository->getOrCreate($recipientId);
        $transaction = $this->getDi()->transactionRepository->getById($transactionId);

        $this->assertCount(1, $this->getDi()->balanceChangingListener->getInvokedEvents());
        $firedEvent = $this->getDi()->balanceChangingListener->getInvokedEvents()[0];
        /** @var BalanceIncreasedEvent $firedEvent */
        $this->assertInstanceOf(BalanceIncreasedEvent::class, $firedEvent);
        $this->assertEquals($transaction, $firedEvent->getTransaction());

        $this->assertEquals($amount, $user->getBalance());
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals($transactionId, $transaction->getId());
        $this->assertEquals($senderId, $transaction->getSenderId());
        $this->assertEquals($recipientId, $transaction->getRecipientId());
        $this->assertEquals($isBlocked, $transaction->getisBlocked());

        // next transaction doubles user balance
        $this->getDi()->balanceManager->processTransaction(
            $transactionId + 1,
            $senderId,
            $recipientId,
            $amount,
            $isBlocked
        );

        $user = $this->getDi()->userRepository->getOrCreate($recipientId);
        $transaction = $this->getDi()->transactionRepository->getById($transactionId + 1);

        // second event dispatched
        $this->assertCount(2, $this->getDi()->balanceChangingListener->getInvokedEvents());
        $firedEvent = $this->getDi()->balanceChangingListener->getInvokedEvents()[1];
        /** @var BalanceIncreasedEvent $firedEvent */
        $this->assertInstanceOf(BalanceIncreasedEvent::class, $firedEvent);
        $this->assertEquals($transaction, $firedEvent->getTransaction());

        $this->assertNotNull($transaction);
        $this->assertEquals($amount * 2, $user->getBalance());

        // next transaction is blocked - user balance stays unchanged
        $this->getDi()->balanceManager->processTransaction(
            $transactionId + 2,
            $senderId,
            $recipientId,
            $amount,
            1
        );

        $user = $this->getDi()->userRepository->getOrCreate($recipientId);
        $transaction = $this->getDi()->transactionRepository->getById($transactionId + 2);

        // no new event dispatched
        $this->assertCount(2, $this->getDi()->balanceChangingListener->getInvokedEvents());

        $this->assertNotNull($transaction);
        $this->assertEquals($amount * 2, $user->getBalance());
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals(1, $transaction->getisBlocked());
    }

    /**
     * Test decreasing user balance
     *
     * @return void
     *
     * @covers ::processTransaction()
     */
    public function testDecreaseBalance(): void
    {
        $transactionId = 100;
        $senderId = 200;
        $recipientId = 0;
        $amount = 100;
        $userInitialBalance = 200;
        $isBlocked = 0;

        $this->getDi()->userRepository->getOrCreate($senderId);
        $this->getDi()->database->master->query(
            'UPDATE users SET balance=? WHERE id=?',
            [
                $userInitialBalance,
                $senderId
            ]
        );

        $transaction = $this->getDi()->transactionRepository->getById($transactionId);
        $this->assertNull($transaction);

        // decreasing user balance
        $this->getDi()->balanceManager->processTransaction(
            $transactionId,
            $senderId,
            $recipientId,
            $amount,
            $isBlocked
        );

        $user = $this->getDi()->userRepository->getOrCreate($senderId);
        $transaction = $this->getDi()->transactionRepository->getById($transactionId);

        $this->assertCount(1, $this->getDi()->balanceChangingListener->getInvokedEvents());
        $firedEvent = $this->getDi()->balanceChangingListener->getInvokedEvents()[0];
        /** @var BalanceDecreasedEvent $firedEvent */
        $this->assertInstanceOf(BalanceDecreasedEvent::class, $firedEvent);
        $this->assertEquals($transaction, $firedEvent->getTransaction());

        $this->assertEquals($userInitialBalance - $amount, $user->getBalance());
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals($transactionId, $transaction->getId());
        $this->assertEquals($senderId, $transaction->getSenderId());
        $this->assertEquals($recipientId, $transaction->getRecipientId());
        $this->assertEquals($isBlocked, $transaction->getisBlocked());

        // decreasing user balance with block
        $this->getDi()->balanceManager->processTransaction(
            $transactionId + 1,
            $senderId,
            $recipientId,
            $amount,
            1
        );

        $user = $this->getDi()->userRepository->getOrCreate($senderId);
        $transaction = $this->getDi()->transactionRepository->getById($transactionId + 1);

        $this->assertCount(2, $this->getDi()->balanceChangingListener->getInvokedEvents());
        $firedEvent = $this->getDi()->balanceChangingListener->getInvokedEvents()[1];
        /** @var BalanceDecreasedEvent $firedEvent */
        $this->assertInstanceOf(BalanceDecreasedEvent::class, $firedEvent);
        $this->assertEquals($transaction, $firedEvent->getTransaction());

        // user's balance decreased
        $this->assertEquals($userInitialBalance - $amount - $amount, $user->getBalance());
        // transaction created with blocked flag
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals($transactionId + 1, $transaction->getId());
        $this->assertEquals($senderId, $transaction->getSenderId());
        $this->assertEquals($recipientId, $transaction->getRecipientId());
        $this->assertEquals(1, $transaction->getisBlocked());
    }

    /**
     * Test transfer and transfer with block
     *
     * @return void
     *
     * @covers ::processTransaction()
     */
    public function testTransfer(): void
    {
        $transactionId = 200;
        $senderId = 300;
        $recipientId = 600;
        $amount = 100;
        $senderInitialBalance = 200;
        $recipientInitialBalance = 0;
        $isBlocked = 0;

        $this->getDi()->userRepository->getOrCreate($senderId);
        $this->getDi()->database->master->query(
            'UPDATE users SET balance=? WHERE id=?',
            [
                $senderInitialBalance,
                $senderId
            ]
        );

        $this->getDi()->database->master->query(
            'UPDATE users SET balance=? WHERE id=?',
            [
                $recipientInitialBalance,
                $recipientId
            ]
        );

        $transaction = $this->getDi()->transactionRepository->getById($transactionId);
        $this->assertNull($transaction);

        // transfer from sender to recipient
        $this->getDi()->balanceManager->processTransaction(
            $transactionId,
            $senderId,
            $recipientId,
            $amount,
            $isBlocked
        );

        $sender = $this->getDi()->userRepository->getOrCreate($senderId);
        $recipient = $this->getDi()->userRepository->getOrCreate($recipientId);
        $transaction = $this->getDi()->transactionRepository->getById($transactionId);


        // 3 events fired - for increase, decrease and transfer
        $this->assertCount(3, $this->getDi()->balanceChangingListener->getInvokedEvents());
        $firedEventDecrease = $this->getDi()->balanceChangingListener->getInvokedEvents()[0];
        $firedEventIncrease = $this->getDi()->balanceChangingListener->getInvokedEvents()[1];
        $firedEventTransfer = $this->getDi()->balanceChangingListener->getInvokedEvents()[2];
        /** @var BalanceDecreasedEvent $firedEvent */
        $this->assertInstanceOf(BalanceDecreasedEvent::class, $firedEventDecrease);
        $this->assertInstanceOf(BalanceIncreasedEvent::class, $firedEventIncrease);
        $this->assertInstanceOf(BalanceTransferredEvent::class, $firedEventTransfer);
        $this->assertEquals($transaction, $firedEventDecrease->getTransaction());
        $this->assertEquals($transaction, $firedEventIncrease->getTransaction());
        $this->assertEquals($transaction, $firedEventTransfer->getTransaction());

        $this->assertEquals($senderInitialBalance - $amount, $sender->getBalance());
        $this->assertEquals($recipientInitialBalance + $amount, $recipient->getBalance());
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals($transactionId, $transaction->getId());
        $this->assertEquals($senderId, $transaction->getSenderId());
        $this->assertEquals($recipientId, $transaction->getRecipientId());
        $this->assertEquals($isBlocked, $transaction->getisBlocked());

        // transfer with block
        $this->getDi()->balanceManager->processTransaction(
            $transactionId + 1,
            $senderId,
            $recipientId,
            $amount,
            1
        );

        $sender = $this->getDi()->userRepository->getOrCreate($senderId);
        $recipient = $this->getDi()->userRepository->getOrCreate($recipientId);
        $transaction = $this->getDi()->transactionRepository->getById($transactionId + 1);

        // 2 events fired - for decrease and transfer. Increase not happened yet because transaction is blocked
        $this->assertCount(5, $this->getDi()->balanceChangingListener->getInvokedEvents());
        $firedEventDecrease = $this->getDi()->balanceChangingListener->getInvokedEvents()[3];
        $firedEventTransfer = $this->getDi()->balanceChangingListener->getInvokedEvents()[4];
        /** @var BalanceDecreasedEvent $firedEvent */
        $this->assertInstanceOf(BalanceDecreasedEvent::class, $firedEventDecrease);
        $this->assertInstanceOf(BalanceTransferredEvent::class, $firedEventTransfer);
        $this->assertEquals($transaction, $firedEventDecrease->getTransaction());
        $this->assertEquals($transaction, $firedEventTransfer->getTransaction());

        // sender balance increased
        $this->assertEquals($senderInitialBalance - $amount - $amount, $sender->getBalance());
        // recipient balance bot increased - its blocked by transaction
        $this->assertEquals($recipientInitialBalance + $amount, $recipient->getBalance());
        $this->assertEquals($amount, $transaction->getAmount());
        $this->assertEquals($transactionId + 1, $transaction->getId());
        $this->assertEquals($senderId, $transaction->getSenderId());
        $this->assertEquals($recipientId, $transaction->getRecipientId());
        $this->assertEquals(1, $transaction->getisBlocked());
    }

    /**
     * Test processing same transaction twice
     *
     * @return void
     *
     * @covers ::processTransaction()
     */
    public function testSameTransaction(): void
    {
        $transactionId = 300;
        $senderId = 0;
        $recipientId = 400;
        $amount = 100;
        $isBlocked = 0;

        $this->getDi()->balanceManager->processTransaction(
            $transactionId,
            $senderId,
            $recipientId,
            $amount,
            $isBlocked
        );

        $this->expectExceptionMessage(sprintf('Duplicate transaction id %d', $transactionId));
        $this->expectException(DuplicateTransactionException::class);

        // same transaction id again will generate exception
        $this->getDi()->balanceManager->processTransaction(
            $transactionId,
            $senderId,
            $recipientId,
            $amount,
            $isBlocked
        );
    }
}

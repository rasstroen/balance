<?php

namespace Balance\Events;

class BalanceTransferredEvent extends AbstractBalanceEvent
{
    /**
     * {@inheritdoc}
     */
    public static function getEventName(): string
    {
        return 'balance.transfer';
    }
}

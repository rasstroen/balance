<?php

namespace Balance\Events;

use Balance\Models\Transaction;

/**
 * Class AbstractBalanceEvent
 *
 * Event for balance changing
 */
abstract class AbstractBalanceEvent extends BaseEvent
{
    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * BalanceDecreasedEvent constructor.
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return Transaction
     */
    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }
}

<?php

namespace Balance\Events\Listeners;

use Balance\Events\BalanceDecreasedEvent;
use Balance\Events\BalanceIncreasedEvent;
use Balance\Events\BalanceTransferredEvent;
use Balance\Events\BaseEvent;

/**
 * Class BalanceChangingListener
 * Listener for events with users balance. @todo do something with this events, not used now
 */
class BalanceChangingListener
{
    /**
     * Invoked events, just for testing
     * @var array
     */
    private $invokedEvents = [];

    /**
     * @return array
     */
    public function getInvokedEvents(): array
    {
        return $this->invokedEvents;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(BaseEvent $event)
    {
        if ($event instanceof BalanceDecreasedEvent) {
            // we can do something with $event->getTransaction();
        } elseif ($event instanceof BalanceIncreasedEvent) {
            // we can do something with $event->getTransaction();
        } elseif ($event instanceof BalanceTransferredEvent) {
            // we can do something with $event->getTransaction();
        } else {
            throw new \Exception('Event type not supported');
        }
        $this->invokedEvents[] = $event;
    }
}

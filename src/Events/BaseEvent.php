<?php

namespace Balance\Events;

use Symfony\Component\EventDispatcher\Event;

abstract class BaseEvent extends Event
{
    /**
     * Gets name of event
     *
     * @return string
     */
    abstract public static function getEventName(): string;
}

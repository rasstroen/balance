<?php

namespace Balance\Events;

class BalanceIncreasedEvent extends AbstractBalanceEvent
{
    /**
     * {@inheritdoc}
     */
    public static function getEventName(): string
    {
        return 'balance.increase';
    }
}

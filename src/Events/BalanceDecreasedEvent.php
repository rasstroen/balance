<?php

namespace Balance\Events;

class BalanceDecreasedEvent extends AbstractBalanceEvent
{
    /**
     * {@inheritdoc}
     */
    public static function getEventName(): string
    {
        return 'balance.decrease';
    }
}

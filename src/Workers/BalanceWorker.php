<?php

namespace Balance\Workers;

class BalanceWorker extends Worker
{
    public const QUEUE_NAME = 'usersBalance';

    /**
     * @inheritdoc
     */
    public function process(array $params): void
    {
        $recipientId = !empty($params['recipientId']) ? (int)$params['recipientId'] : 0;
        $senderId = !empty($params['senderId']) ? (int)$params['senderId'] : 0;
        $amount = !empty($params['amount']) ? (int)$params['amount'] : null;
        $isBlocked = !empty($params['isBlocked']) ? (bool)$params['isBlocked'] : false;
        $transactionId = !empty($params['transactionId']) ? (int)$params['transactionId'] : null;

        if (!$recipientId && !$senderId) {
            throw new \Exception('Sender and recipient are not provided');
        }

        if (!$amount) {
            throw new \Exception('Amount is not provided');
        }

        if (!$transactionId) {
            throw new \Exception('Transaction id is not provided');
        }

        $this->getDi()->balanceManager->processTransaction(
            $transactionId,
            $senderId,
            $recipientId,
            $amount,
            $isBlocked
        );

        if ($this->getOutput()) {
            $this->getOutput()->writeln(sprintf('Processed transaction %d', $transactionId));
        }
    }

    /**
     * @inheritdoc
     */
    protected function getQueueName(): string
    {
        return self::QUEUE_NAME;
    }
}

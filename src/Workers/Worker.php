<?php

namespace Balance\Workers;

use Balance\Components\ServiceLocator\ServiceLocator;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Worker
{
    /**
     * @var ServiceLocator
     */
    private $dependencyInjection;

    /**
     * @var OutputInterface|null
     */
    private $output;

    public function __construct(ServiceLocator $dependencyInjection, OutputInterface $output = null)
    {
        $this->dependencyInjection = $dependencyInjection;
        $this->output = $output;
    }

    /**
     * @return OutputInterface|null
     */
    public function getOutput(): ?OutputInterface
    {
        return $this->output;
    }

    /**
     * @return ServiceLocator
     */
    protected function getDi(): ServiceLocator
    {
        return $this->dependencyInjection;
    }

    /**
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPQueueException
     */
    public function run()
    {
        $queue = $this->getDi()->amqp->getQueue($this->getQueueName());
        $queue->consume([$this, 'processTask']);
    }

    /**
     * @param \AMQPEnvelope $envelop
     * @return bool
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPQueueException
     */
    public function processTask(\AMQPEnvelope $envelop)
    {
        $params = json_decode($envelop->getBody(), true);
        $this->getDi()->amqp->getQueue($this->getQueueName())->ack($envelop->getDeliveryTag());
        $this->process($params);
        return true;
    }

    abstract public function process(array $params): void;

    abstract protected function getQueueName(): string;
}

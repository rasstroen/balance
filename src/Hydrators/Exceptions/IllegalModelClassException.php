<?php

namespace Balance\Hydrators\Exceptions;

class IllegalModelClassException extends \Exception
{
}

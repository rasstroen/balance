<?php

namespace Balance\Hydrators;

use Balance\Models\Model;

interface BaseHydrator
{
    /**
     * @param Model $model
     * @param array $data
     *
     * @return Model
     */
    public function hydrate(Model $model, array $data) : Model;
}

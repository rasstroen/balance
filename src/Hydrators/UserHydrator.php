<?php

namespace Balance\Hydrators;

use Balance\Hydrators\Exceptions\IllegalModelClassException;
use Balance\Models\Model;
use Balance\Models\User;

/**
 * @todo tests
 *
 * Class UserHydrator
 * @package Balance\Hydrators
 */
class UserHydrator implements BaseHydrator
{
    /**
     * @param Model $model
     * @param array $data
     *
     * @throws IllegalModelClassException
     *
     * @return Model
     */
    public function hydrate(Model $model, array $data): Model
    {
        if (!$model instanceof User) {
            throw new IllegalModelClassException(
                sprintf(
                    'expected %s but %s passed',
                    User::class,
                    get_class($model)
                )
            );
        }

        $model
            ->setId((int)$data['id'])
            ->setBalance((int)$data['balance']);

        return $model;
    }
}

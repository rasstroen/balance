<?php

namespace Balance\Hydrators;

use Balance\Hydrators\Exceptions\IllegalModelClassException;
use Balance\Models\Model;
use Balance\Models\Transaction;
use Balance\Models\User;

/**
 * @todo tests
 *
 * Class UserHydrator
 * @package Balance\Hydrators
 */
class TransactionHydrator implements BaseHydrator
{
    /**
     * @param Model $model
     * @param array $data
     *
     * @throws IllegalModelClassException
     *
     * @return Model
     */
    public function hydrate(Model $model, array $data): Model
    {
        if (!$model instanceof Transaction) {
            throw new IllegalModelClassException(
                sprintf(
                    'expected %s but %s passed',
                    Transaction::class,
                    get_class($model)
                )
            );
        }

        $model
            ->setId((int)$data['id'])
            ->setSenderId((int)$data['senderId'])
            ->setRecipientId((int)$data['recipientId'])
            ->setAmount((int)$data['amount'])
            ->setIsBlocked((int)$data['isBlocked'])
            ->setType((int)$data['type'])
            ->setTimestamp((int)$data['timestamp'])
            ->setDescription($data['description'] ?? '');

        return $model;
    }
}

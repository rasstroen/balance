<?php

namespace Balance\Repositories;

use Balance\Models\User;

class UserRepository extends BaseRepository
{
    protected function getTableName(): string
    {
        return 'users';
    }

    public function increaseBalance(User $user, $amount)
    {
        $this->getDatabase()->master->query(
            'UPDATE `users` SET `balance`=`balance`+? WHERE `id`=?',
            [$amount, $user->getId()]
        );
    }

    public function decreaseBalance(User $user, $amount, $oldBalance): int
    {
        return $this->getDatabase()->master->query(
            'UPDATE `users` SET `balance` = `balance` - ? WHERE `id` = ? AND `balance` = ?',
            [$amount, $user->getId(), $oldBalance]
        )->rowCount();
    }

    public function getOrCreateForUpdate(int $userId): User
    {
        $userData = $this->getDatabase()->master->selectRow('SELECT * FROM `users` WHERE `id` = ?  FOR UPDATE', [$userId]);
        if (!$userData) {
            $this->getDatabase()->master->insert($this->getTableName(), ['id' => $userId, 'balance' => 0], ['id']);
            $userData = $this->getDatabase()->master->selectRow('SELECT * FROM `users` WHERE `id` = ?  FOR UPDATE', [$userId]);
        }

        return $this->getHydrator()->hydrate(new User(), $userData);
    }

    public function getOrCreate(int $userId): User
    {
        $userData = $this->getDatabase()->master->selectRow('SELECT * FROM `users` WHERE `id` = ?', [$userId]);
        if (!$userData) {
            $this->getDatabase()->master->insert($this->getTableName(), ['id' => $userId, 'balance' => 0], ['id']);
            $userData = $this->getDatabase()->master->selectRow('SELECT * FROM `users` WHERE `id` = ?', [$userId]);
        }

        return $this->getHydrator()->hydrate(new User(), $userData);
    }
}

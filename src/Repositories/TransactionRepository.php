<?php

namespace Balance\Repositories;

use Balance\Models\Transaction;

class TransactionRepository extends BaseRepository
{
    protected function getTableName(): string
    {
        return 'transactions';
    }

    /**
     * @param int $id
     * @return \Balance\Models\Transaction|null
     */
    public function getById(int $id): ?Transaction
    {
        $row = $this->getDatabase()->master->selectRow('SELECT * FROM transactions WHERE id = ?', [$id]);
        return $row ? $this->getHydrator()->hydrate(new Transaction(), $row) : null;
    }

    /**
     * @param int $id
     * @param int $senderId
     * @param int $recipientId
     * @param int $amount
     * @param int $type
     * @param int $isBlocked
     * @param int $timestamp
     * @param string $description
     * @throws \Doctrine\DBAL\DBALException
     */
    public function create(
        int $id,
        int $senderId,
        int $recipientId,
        int $amount,
        int $type,
        int $isBlocked = 0,
        int $timestamp = 0,
        string $description = ''
    ) {
        $timestamp = $timestamp ?? time();

        $data = [
            'id' => $id,
            'senderId' => $senderId,
            'recipientId' => $recipientId,
            'amount' => $amount,
            'type' => $type,
            'isBlocked' => $isBlocked,
            'timestamp' => $timestamp,
            'description' => $description
        ];
        $this->getDatabase()->master->insert(
            $this->getTableName(),
            $data
        );

        return $this->getHydrator()->hydrate(new Transaction(), $data);
    }
}

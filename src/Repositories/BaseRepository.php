<?php

namespace Balance\Repositories;

use Balance\Components\Database\DatabaseFactory;
use Balance\Hydrators\BaseHydrator;

abstract class BaseRepository
{
    /**
     * @var BaseHydrator
     */
    private $hydrator;

    /**
     * @var DatabaseFactory
     */
    private $database;

    public function __construct(BaseHydrator $hydrator, DatabaseFactory $database)
    {
        $this->database = $database;
        $this->hydrator = $hydrator;
    }

    /**
     * @return BaseHydrator
     */
    public function getHydrator(): BaseHydrator
    {
        return $this->hydrator;
    }

    /**
     * @return DatabaseFactory
     */
    public function getDatabase(): DatabaseFactory
    {
        return $this->database;
    }

    abstract protected function getTableName(): string;
}

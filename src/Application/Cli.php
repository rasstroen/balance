<?php

namespace Balance\Application;

use Balance\Commands\Base;
use Balance\Components\ServiceLocator\ServiceLocator;
use Symfony\Component\Console\Application;

/**
 * Class Cli
 * Cli application to run commands
 */
class Cli
{
    /**
     * Project configuration array
     *
     * @var array
     */
    private $configuration;

    /**
     * Symfony console application
     *
     * @var Application
     */
    private $application;

    /**
     * Container for services
     *
     * @var ServiceLocator
     */
    private $dependencyInjection;

    /**
     * Cli constructor.
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return ServiceLocator
     */
    protected function getDi(): ServiceLocator
    {
        return $this->dependencyInjection;
    }

    /**
     * Runs application
     */
    public function run(): void
    {
        $this->initApplication()->run();
    }

    /**
     * Initialize symfony application, adds commands
     *
     * @return Application
     */
    private function initApplication(): Application
    {
        $this->application = new Application();

        $this->dependencyInjection = new ServiceLocator($this->configuration);

        foreach ($this->configuration['commands'] as $commandClass) {
            $command = new $commandClass($this->getDi());
            if ($command instanceof Base) {
                $this->application->add($command);
            }
        }

        return $this->application;
    }
}

<?php

namespace Balance\Services;

use Balance\Components\Database\DatabaseFactory;
use Balance\Events\BalanceDecreasedEvent;
use Balance\Events\BalanceIncreasedEvent;
use Balance\Events\BalanceTransferredEvent;
use Balance\Models\Transaction;
use Balance\Repositories\TransactionRepository;
use Balance\Repositories\UserRepository;
use Balance\Services\Exceptions\DuplicateTransactionException;
use Doctrine\DBAL\DBALException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class BalanceManager
 * Uses for managing users balance (increase/decrease/transfer) with ability to block transaction to process later
 */
class BalanceManager
{
    /**
     * @var DatabaseFactory
     */
    private $database;

    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * BalanceManager constructor.
     * @param DatabaseFactory $database
     * @param TransactionRepository $transactionRepository
     * @param UserRepository $userRepository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        DatabaseFactory $database,
        TransactionRepository $transactionRepository,
        UserRepository $userRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->database = $database;
        $this->transactionRepository = $transactionRepository;
        $this->userRepository = $userRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param int $transactionId
     * @param int $senderId
     * @param int $recipientId
     * @param int $amount
     * @param int $isBlocked
     * @return Transaction
     * @throws \Exception
     */
    public function processTransaction(
        int $transactionId,
        int $senderId,
        int $recipientId,
        int $amount,
        int $isBlocked
    ): Transaction {
        $transaction = null;
        try {
            $this->database->master->beginTransaction();
            if ($senderId && $recipientId && $amount) {
                $transaction = $this->transferBalance($transactionId, $senderId, $recipientId, $amount, $isBlocked);
            } elseif ($senderId && $amount) {
                $transaction = $this->decreaseBalance($transactionId, $senderId, $amount, $isBlocked);
            } elseif ($recipientId && $amount) {
                $transaction = $this->increaseBalance($transactionId, $recipientId, $amount, $isBlocked);
            } else {
                $this->database->master->rollback();
                throw new \Exception('Action cant be recognized');
            }
            $this->database->master->commit();
        } catch (DBALException $exception) {
            if (strpos($exception->getMessage(), 'Duplicate entry')) {
                $this->database->master->rollback();
                throw new DuplicateTransactionException(sprintf('Duplicate transaction id %d', $transactionId));
            }
            throw $exception;
        }

        return $transaction;
    }

    private function increaseBalance(int $transactionId, int $recipientId, int $amount, int $isBlocked)
    {
        // create transaction
        $transaction = $this->transactionRepository->create(
            $transactionId,
            0,
            $recipientId,
            $amount,
            Transaction::TYPE_GAIN,
            $isBlocked,
            time(),
            'increase balance'
        );

        $recipient = $this->userRepository->getOrCreate($recipientId);
        if (!$isBlocked) {
            $this->userRepository->increaseBalance($recipient, $amount);
            $this->eventDispatcher->dispatch(BalanceIncreasedEvent::getEventName(), new BalanceIncreasedEvent($transaction));
        }

        return $transaction;
    }

    private function decreaseBalance(int $transactionId, int $senderId, int $amount, int $isBlocked)
    {
        // create transaction
        $transaction = $this->transactionRepository->create(
            $transactionId,
            $senderId,
            0,
            $amount,
            Transaction::TYPE_SPEND,
            $isBlocked,
            time(),
            'decrease balance'
        );

        $sender = $this->userRepository->getOrCreateForUpdate($senderId);
        if ($sender->getBalance() < $amount) {
            $this->database->master->rollback();
            throw new \Exception('Infucient balance');
        }
        $this->userRepository->decreaseBalance($sender, $amount, $sender->getBalance());

        $this->eventDispatcher->dispatch(BalanceDecreasedEvent::getEventName(), new BalanceDecreasedEvent($transaction));

        return $transaction;
    }

    /**
     * @param int $transactionId
     * @param int $senderId
     * @param int $recipientId
     * @param int $amount
     * @param int $isBlocked
     * @throws \Doctrine\DBAL\DBALException
     */
    private function transferBalance(
        int $transactionId,
        int $senderId,
        int $recipientId,
        int $amount,
        int $isBlocked
    ) {
        // create transaction
        $transaction = $this->transactionRepository->create(
            $transactionId,
            $senderId,
            $recipientId,
            $amount,
            Transaction::TYPE_TRANSFER,
            $isBlocked,
            time(),
            'transfer'
        );
        // get/create recipient
        $recipient = $this->userRepository->getOrCreate($recipientId);
        // get/create sender
        $sender = $this->userRepository->getOrCreateForUpdate($senderId);
        // decrease sender balance if can
        if ($sender->getBalance() < $amount) {
            $this->database->master->rollback();
            throw new \Exception('Infucient balance');
        }
        $this->userRepository->decreaseBalance($sender, $amount, $sender->getBalance());
        $this->eventDispatcher->dispatch(BalanceDecreasedEvent::getEventName(), new BalanceDecreasedEvent($transaction));
        // increase recipient balance if not blocked
        if (!$isBlocked) {
            $this->eventDispatcher->dispatch(BalanceIncreasedEvent::getEventName(), new BalanceIncreasedEvent($transaction));
            $this->userRepository->increaseBalance($recipient, $amount);
        }

        $this->eventDispatcher->dispatch(BalanceTransferredEvent::getEventName(), new BalanceTransferredEvent($transaction));

        return $transaction;
    }
}

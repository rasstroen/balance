<?php

namespace Balance\Services\Exceptions;

use Exception;

class DuplicateTransactionException extends Exception
{
}

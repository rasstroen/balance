<?php

namespace Balance\Commands;

use Balance\Workers\BalanceWorker;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Worker
 * Command to run worker. This should be under supervisor
 */
class Worker extends Base
{
    protected function configure(): void
    {
        $this
            ->setName('worker:balance:management')
            ->setDescription('performs user\'s balcnce changing ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Starting worker to process balance changing requests');

        $worker = (new BalanceWorker($this->getDi(), $output));

        while (true) {
            $worker->run();
        }
    }
}

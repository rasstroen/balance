<?php

namespace Balance\Commands;

use Balance\Components\ServiceLocator\ServiceLocator;
use Symfony\Component\Console\Command\Command;

/**
 * Class Base
 * Command base
 */
class Base extends Command
{
    /**
     * @var ServiceLocator
     */
    private $dependencyInjection;

    /**
     * Base constructor.
     * @param ServiceLocator $dependencyInjection
     */
    public function __construct(ServiceLocator $dependencyInjection)
    {
        parent::__construct();
        $this->dependencyInjection = $dependencyInjection;
    }

    /**
     * @return ServiceLocator
     */
    protected function getDi(): ServiceLocator
    {
        return $this->dependencyInjection;
    }
}

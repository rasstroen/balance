<?php

namespace Balance\Commands;

use Balance\Workers\BalanceWorker;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendEvent
 * Command adds event to queue. Used only for testing/presentation,
 * normal case - to receive events directly to queue from other microservices
 */
class SendEvent extends Base
{
    protected function configure(): void
    {
        $this
            ->setName('worker:send:event')
            ->setDescription('performs user\'s balcnce changing ')
            ->addArgument('recipientId', InputArgument::REQUIRED)
            ->addArgument('senderId', InputArgument::REQUIRED)
            ->addArgument('amount', InputArgument::REQUIRED)
            ->addArgument('isBlocked', InputArgument::REQUIRED)
            ->addArgument('transactionId', InputArgument::REQUIRED);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $recipientId = (int)$input->getArgument('recipientId');
        $senderId = (int)$input->getArgument('senderId');
        $amount = (int)$input->getArgument('amount');
        $isBlocked = (bool)$input->getArgument('isBlocked');
        $transactionId = (int)$input->getArgument('transactionId');

        $output->writeln(
            sprintf(
                'Sending event: recipientId:%d, senderId:%d, amount %d, isBlocked: %d, transactionId: %d',
                $recipientId,
                $senderId,
                $amount,
                $isBlocked,
                $transactionId
            )
        );

        $result = $this->getDi()->amqp->send(
            BalanceWorker::QUEUE_NAME,
            [
                'recipientId' => $recipientId,
                'senderId' => $senderId,
                'amount' => $amount,
                'isBlocked' => $isBlocked,
                'transactionId' => $transactionId
            ]
        );

        if ($result) {
            $output->write('done');
        }
    }
}

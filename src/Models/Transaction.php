<?php

namespace Balance\Models;

class Transaction implements Model
{
    /**
     * User balance decreasing
     *
     * @const int
     */
    public const TYPE_SPEND = 1;

    /**
     * User balance increasing
     *
     * @const int
     */
    public const TYPE_GAIN = 2;

    /**
     * Transfer from sender to recipient
     *
     * @const int
     */
    public const TYPE_TRANSFER = 3;
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $senderId;

    /**
     * @var int
     */
    private $recipientId;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var int
     */
    private $isBlocked;

    /**
     * @var int
     */
    private $type;

    /**
     * @var int
     */
    private $timestamp;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Transaction
     */
    public function setId(int $id): Transaction
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSenderId(): ?int
    {
        return $this->senderId;
    }

    /**
     * @return int
     */
    public function getisBlocked(): int
    {
        return $this->isBlocked;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     * @return Transaction
     */
    public function setTimestamp(int $timestamp): Transaction
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @param int $isBlocked
     * @return Transaction
     */
    public function setIsBlocked(int $isBlocked): Transaction
    {
        $this->isBlocked = $isBlocked;
        return $this;
    }

    /**
     * @param int|null $senderId
     * @return Transaction
     */
    public function setSenderId(?int $senderId): Transaction
    {
        $this->senderId = $senderId;
        return $this;
    }

    /**
     * @return int
     */
    public function getRecipientId(): int
    {
        return $this->recipientId;
    }

    /**
     * @param int $recipientId
     * @return Transaction
     */
    public function setRecipientId(int $recipientId): Transaction
    {
        $this->recipientId = $recipientId;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Transaction
     */
    public function setAmount(int $amount): Transaction
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Transaction
     */
    public function setType(int $type): Transaction
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return Transaction
     */
    public function setDescription(?string $description): Transaction
    {
        $this->description = $description;
        return $this;
    }
}

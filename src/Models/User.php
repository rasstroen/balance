<?php

namespace Balance\Models;

class User implements Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $balance;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     * @return User
     */
    public function setBalance(int $balance): User
    {
        $this->balance = $balance;
        return $this;
    }
}

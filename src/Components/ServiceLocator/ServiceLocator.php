<?php

namespace Balance\Components\ServiceLocator;

use Balance\Components\Amqp\AmqpClient;
use Balance\Components\Database\DatabaseFactory;
use Balance\Components\EventDispatcher\EventDispatcherFactory;
use Balance\Events\Listeners\BalanceChangingListener;
use Balance\Hydrators\TransactionHydrator;
use Balance\Hydrators\UserHydrator;
use Balance\Repositories\TransactionRepository;
use Balance\Repositories\UserRepository;
use Balance\Services\BalanceManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ServiceLocator
 * All services are here
 *
 * @property DatabaseFactory $database
 * @property AmqpClient $amqp
 * @property UserRepository $userRepository
 * @property TransactionRepository $transactionRepository
 * @property UserHydrator $userHydrator
 * @property TransactionHydrator $transactionHydrator
 * @property BalanceManager $balanceManager
 * @property EventDispatcherInterface $eventDispatcher
 * @property BalanceChangingListener $balanceChangingListener
 */
class ServiceLocator
{
    /**
     * @var array
     */
    private $services = [];

    /**
     * @var array
     */
    private $servicesConfig = [];

    public function __construct(array $configuration)
    {
        $this->addService('database', function () use ($configuration): DatabaseFactory {
            return new DatabaseFactory($configuration['database']);
        });

        $this->addService(
            'amqp',
            function () use ($configuration) : AmqpClient {
                return new AmqpClient($configuration['amqp']);
            }
        );
        $this->addService(
            'userHydrator',
            function (): UserHydrator {
                return new UserHydrator();
            }
        );
        $this->addService(
            'transactionHydrator',
            function (): TransactionHydrator {
                return new TransactionHydrator();
            }
        );
        $this->addService(
            'balanceChangingListener',
            function (): BalanceChangingListener {
                return new BalanceChangingListener();
            }
        );
        $this->addService(
            'eventDispatcher',
            function (): EventDispatcherInterface {
                return (new EventDispatcherFactory())->build($this);
            }
        );
        $this->addService(
            'userRepository',
            function (): UserRepository {
                return new UserRepository(
                $this->userHydrator,
                $this->database
            );
            }
        );
        $this->addService(
            'transactionRepository',
            function (): TransactionRepository {
                return new TransactionRepository(
                $this->transactionHydrator,
                $this->database
            );
            }
        );
        $this->addService(
            'balanceManager',
            function (): BalanceManager {
                return new BalanceManager(
                $this->database,
                $this->transactionRepository,
                $this->userRepository,
                $this->eventDispatcher
            );
            }
        );
    }

    /**
     * Gets service by name
     *
     * @param string $serviceName
     * @return mixed|null
     */
    public function __get(string $serviceName)
    {
        return $this->getServiceByName($serviceName);
    }

    /**
     * Adds service
     *
     * @param string $name
     * @param callable|object $service
     */
    public function addService(string $name, $service): void
    {
        $this->servicesConfig[$name] = $service;
    }

    /**
     * Gets service by name
     *
     * @param string $serviceName
     * @return mixed|null
     */
    private function getServiceByName(string $serviceName)
    {
        return $this->services[$serviceName] ?? $this->createService($serviceName);
    }

    /**
     * Creates service
     *
     * @param string $serviceName
     * @return mixed|null
     */
    private function createService(string $serviceName)
    {
        $serviceConfig = $this->servicesConfig[$serviceName] ?? null;

        if ($serviceConfig) {
            $this->services[$serviceName] = $serviceConfig();
        }

        return $this->services[$serviceName] ?? null;
    }
}

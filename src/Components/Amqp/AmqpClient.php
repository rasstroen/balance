<?php

namespace Balance\Components\Amqp;

/**
 * Class AmqpClient
 * Client to interact with amqp
 */
class AmqpClient
{
    /**
     * @var array
     */
    private $configuration;

    /**
     * @var \AMQPChannel
     */
    private $channel;

    /**
     * @var \AMQPQueue[]
     */
    private $queues;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
        $this->connect();
    }

    /**
     * @throws \AMQPConnectionException
     */
    private function connect()
    {
        $connection = new \AMQPConnection($this->configuration['server']);
        $connection->connect();

        $this->channel = new \AMQPChannel($connection);
    }

    /**
     * @param string $queueName
     * @param array $payload
     * @return bool
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     */
    public function send(string $queueName, array $payload = []): bool
    {
        return $this->getExchange($queueName)->publish(
            json_encode($payload),
            'route_' . $queueName,
            AMQP_NOPARAM,
            ['delivery_tag' => 1]
        );
    }

    /**
     * @param string $queueName
     * @return \AMQPExchange
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     */
    public function getExchange(string $queueName): \AMQPExchange
    {
        $exchange = new \AMQPExchange($this->channel);
        $exchange->setName('exchange_' . $queueName);
        $exchange->setType(AMQP_EX_TYPE_FANOUT);
        $exchange->setFlags(AMQP_DURABLE);
        $exchange->declareExchange();
        return $exchange;
    }

    /**
     * @param string $queueName
     * @return \AMQPQueue
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     * @throws \AMQPQueueException
     */
    public function getQueue(string $queueName): \AMQPQueue
    {
        if (!isset($this->queues[$queueName])) {
            $queue = new \AMQPQueue($this->channel);
            $queue->setName($queueName);
            $queue->setFlags(AMQP_DURABLE);
            $queue->declareQueue();
            $queue->bind($this->getExchange($queueName)->getName(), 'route_' . $queueName);
            $this->queues[$queueName] = $queue;
        }

        return $this->queues[$queueName];
    }
}

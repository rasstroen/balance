<?php

namespace Balance\Components\EventDispatcher;

use Balance\Components\ServiceLocator\ServiceLocator;
use Balance\Events\BalanceDecreasedEvent;
use Balance\Events\BalanceIncreasedEvent;
use Balance\Events\BalanceTransferredEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Factory to create event dispatcher with all needed listeners
 */
class EventDispatcherFactory
{
    /**
     * @param ServiceLocator $dependencyInjection
     * @return EventDispatcher
     */
    public function build(ServiceLocator $dependencyInjection): EventDispatcher
    {
        $eventDispatcher = new EventDispatcher();

        $eventDispatcher->addListener(BalanceDecreasedEvent::getEventName(), $dependencyInjection->balanceChangingListener);
        $eventDispatcher->addListener(BalanceIncreasedEvent::getEventName(), $dependencyInjection->balanceChangingListener);
        $eventDispatcher->addListener(BalanceTransferredEvent::getEventName(), $dependencyInjection->balanceChangingListener);

        return $eventDispatcher;
    }
}

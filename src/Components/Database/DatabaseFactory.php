<?php

namespace Balance\Components\Database;

use Balance\Components\Database\Exceptions\ConfigurationException;
use Doctrine\DBAL\DriverManager;

/**
 * Class ConnectionsFactory
 *
 * @property Connection $master
 */
class DatabaseFactory
{
    /**
     * @var Connection[]
     */
    private $connections = [];

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param string $connectionName
     * @return array|null
     */
    public function getConnectionSettings(string $connectionName): ?array
    {
        return $this->configuration[$connectionName] ?? null;
    }


    public function __get(string $connectionName): Connection
    {
        if (!isset($this->connections[$connectionName])) {
            $this->connections[$connectionName] = $this->createConnection($connectionName);
        }

        return $this->connections[$connectionName];
    }

    /**
     * @param string $connectionName
     * @throws ConfigurationException
     * @throws \Doctrine\DBAL\DBALException
     * @return Connection
     */
    private function createConnection(string $connectionName): Connection
    {
        $connectionConfig = $this->getConnectionSettings($connectionName);

        if (empty($connectionConfig['dbname'])) {
            throw new ConfigurationException('dbname parameter missed in configuration');
        }

        if (empty($connectionConfig['user'])) {
            throw new ConfigurationException('user parameter missed in configuration');
        }

        $connectionParams = [
            'dbname' => (string)$connectionConfig['dbname'],
            'user' => (string)$connectionConfig['user'],
            'password' => (string)($connectionConfig['password'] ?? ''),
            'host' => (string)($connectionConfig['host'] ?? 'localhost'),
            'driver' => (string)($connectionConfig['driver'] ?? 'mysqli'),
            'charset' => (string)($connectionConfig['charset'] ?? 'utf8'),
        ];
        $connection = new Connection(DriverManager::getConnection($connectionParams));

        if (!empty($connectionParams['charset'])) {
            $connection->query('SET NAMES ' . (string)$connectionParams['charset']);
        }

        return $connection;
    }
}

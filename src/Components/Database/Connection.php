<?php

namespace Balance\Components\Database;

use Doctrine\DBAL\Types\Type;

class Connection
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    public $connection;

    /**
     * Connection constructor.
     * @param \Doctrine\DBAL\Connection $connection
     */
    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param $query
     * @param array $parameters
     * @return \Doctrine\DBAL\Driver\ResultStatement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function query($query, array $parameters = [])
    {
        if ($parameters) {
            $types = $this->getTypes($parameters);
        } else {
            $types = [];
        }
        $result = $this->connection->executeQuery($query, $parameters, $types);

        return $result;
    }


    /**
     * @param string $query
     * @param array $parameters
     * @return array|null
     */
    public function selectRow(string $query, array $parameters = [])
    {
        $types = $this->getTypes($parameters);

        return $this->_query($query, $parameters, $types)->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $tableName
     * @param array $data
     * @param array|null $onDuplicateKeyUpdate
     * @throws \Doctrine\DBAL\DBALException
     * @return int
     */
    public function insert(string $tableName, array $data, array $onDuplicateKeyUpdate = null)
    {
        $params = array_values($data);
        $query = 'INSERT INTO ' . $tableName . ' (' . implode(', ', array_keys($data)) . ')' .
            ' VALUES (' . implode(', ', array_fill(0, count($data), '?')) . ')';

        if ($onDuplicateKeyUpdate) {
            $query .= ' ON DUPLICATE KEY UPDATE ';
            $queryParts = [];

            foreach ($onDuplicateKeyUpdate as $field) {
                $queryParts[] = '`' . $field . '`= ?';
                $params[] = $data[$field];
            }
            $query .= implode(',', $queryParts);
        }

        $result = $this->connection->executeUpdate(
            $query,
            $params,
            $this->getTypes($params)
        );

        return $result;
    }

    /**
     * @param array $parameters
     * @return array
     */
    private function getTypes(array $parameters = [])
    {
        $types = [];

        foreach ($parameters as $parameter) {
            if (is_int($parameter)) {
                $types[] = Type::STRING;
            } elseif (is_array($parameter)) {
                $types[] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;
            } else {
                $types[] = Type::STRING;
            }
        }

        return $types;
    }

    /**
     * @param string $query
     * @param array $parameters
     * @param array $types
     * @throws \Doctrine\DBAL\DBALException
     * @return \Doctrine\DBAL\Driver\Statement
     */
    private function _query(string $query, array $parameters = [], array $types = [])
    {
        return $this->connection->executeQuery($query, $parameters, $types);
    }

    /**
     * Starts transaction
     */
    public function beginTransaction(): void
    {
        $this->connection->beginTransaction();
    }

    /**
     * Commits transaction
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function commit(): void
    {
        $this->connection->commit();
    }

    /**
     * Rollback transaction
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function rollback(): void
    {
        $this->connection->rollBack();
    }
}
